import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { InicioComponent } from './components/inicio/inicio.component';
import { ProductoComponent } from './components/producto/producto.component';
import { ClientesComponent } from './components/clientes/clientes.component';
import { CrearEditarClienteComponent } from './components/crear-editar-cliente/crear-editar-cliente.component';
import { CrearEditarProductoComponent } from './components/crear-editar-producto/crear-editar-producto.component';

const routes: Routes =[
  {path: '', component: InicioComponent},
  {path: 'productos', component: ProductoComponent},
  {path: 'clientes', component: ClientesComponent},
  {path: 'crearclientes', component: CrearEditarClienteComponent},
  {path: 'crearProducto', component: CrearEditarProductoComponent},
  {path: 'editar/:id', component: CrearEditarClienteComponent},
  {path: '**', redirectTo:'', pathMatch: 'full'}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
