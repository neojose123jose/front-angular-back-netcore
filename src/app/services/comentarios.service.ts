import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { cliente } from '../interfaces/comentario';
import { producto } from '../interfaces/comentario';
import { Comentario } from '../interfaces/comentario';

@Injectable({
  providedIn: 'root'
})
export class ComentariosService {
//hacer peticiones http al backend, reutilizacion de codigos entre componentes, comunicacion de datos entre componentes.

  private urlAppback = 'https://localhost:44308/api/';
  //private urlApiback = 'ophel/';
  private urlProducto = 'Producto/';
  private urlCliente = 'Cliente/';

  constructor(private http: HttpClient) { }

  getListValuesProducto(): Observable<any>{
    return this.http.get(this.urlAppback + this.urlProducto);
  }
  delateDataProducto(id: number): Observable<any>{
    return this.http.delete(this.urlAppback + this.urlProducto + id);
  }
  getListValuesCliente(): Observable<any>{
    return this.http.get(this.urlAppback + this.urlCliente);
  }
  delateDataCliente(id: number): Observable<any>{
    return this.http.delete(this.urlAppback + this.urlCliente + id);
  }
  saveDataCliente(data: cliente):Observable<any>{
    return this.http.post(this.urlAppback + this.urlCliente, data);

  }
  updateDataCliente(id: number, data: cliente): Observable<any>{
    return this.http.put(this.urlAppback + this.urlCliente + id, data);
  }
  saveDataProducto(data: producto):Observable<any>{
    return this.http.post(this.urlAppback + this.urlProducto, data);

  }
  updateDataProducto(id: number, data: producto): Observable<any>{
    return this.http.put(this.urlAppback + this.urlProducto + id, data);
  }
  getIdCliente(id: number): Observable<any>{
    //console.log("a");
    //console.log(this.urlAppback + this.urlApiback + id);
    return this.http.get(this.urlAppback + this.urlCliente + id);
  }  
  getIdProducto(id: number): Observable<any>{
    //console.log("a");
    //console.log(this.urlAppback + this.urlApiback + id);
    return this.http.get(this.urlAppback + this.urlProducto + id);
  } 
/*   getListValues(): Observable<any>{
    return this.http.get(this.urlAppback + this.urlApiback);
  }

  delateData(id: number): Observable<any>{
    return this.http.delete(this.urlAppback + this.urlApiback + id);
  }

  saveData(data: Comentario):Observable<any>{
    return this.http.post(this.urlAppback + this.urlApiback, data);

  }
  getId(id: number): Observable<any>{
    //console.log("a");
    //console.log(this.urlAppback + this.urlApiback + id);
    return this.http.get(this.urlAppback + this.urlApiback + id);
  } 
  updateData(id: number, data: Comentario): Observable<any>{
   return this.http.put(this.urlAppback + this.urlApiback + id, data);
  } */

}
