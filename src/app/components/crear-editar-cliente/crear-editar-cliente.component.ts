import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { cliente } from 'src/app/interfaces/comentario';
import { ComentariosService } from 'src/app/services/comentarios.service';

@Component({
  selector: 'app-crear-editar-cliente',
  templateUrl: './crear-editar-cliente.component.html',
  styleUrls: ['./crear-editar-cliente.component.css']
})
export class CrearEditarClienteComponent implements OnInit {
  action: string;
  id: number;
  comentario: cliente | undefined;

  agregarComentario: FormGroup;

  constructor(private fb: FormBuilder,
    private _comentario: ComentariosService,
    private router: Router,
    private aRoute: ActivatedRoute) {
    this.agregarComentario = this.fb.group({
      nombre: ['', Validators.required],
      apellido: ['', Validators.required],
      cedula: ['', Validators.required]

    })
    this.id = +this.aRoute.snapshot.paramMap.get('id')!;
  }

  ngOnInit(): void {
    // this.obtenerInfor();
      this.doEdit();
  }

  doEdit() {
    if (this.id !== 0) {
      this.action = "Editar";
      this._comentario.getIdCliente(this.id).subscribe(data => {
        this.comentario = data;
        this.agregarComentario.patchValue({
          nombre: data.nombre,
          apellido: data.apellido,
          cedula: data.cedula
        })
      })
    } else {

      this.action = "Egregar";
    }
  }
  /*    obtenerInfor() {
      this._comentario.getListValues().subscribe(data => {
        console.log(data);
      }, error => {
        console.log(error);
      })
    }  */

  AddOrEdit() {
    //console.log(this.agregarComentario);
    if (this.comentario == undefined) {
      const Comentario: cliente = {
        nombre: this.agregarComentario.get('nombre')?.value,
        apellido: this.agregarComentario.get('apellido')?.value,
        cedula: this.agregarComentario.get('cedula')?.value,
        fechaCreacion: new Date
      }
      this._comentario.saveDataCliente(Comentario).subscribe(data => { this.router.navigate(['/']); }, error => { console.log(error); })

      console.log(Comentario);
    } else {
      const Comentario: cliente = {
        id: this.comentario.id,
        nombre: this.agregarComentario.get('nombre')?.value,
        apellido: this.agregarComentario.get('apellido')?.value,
        cedula: this.agregarComentario.get('cedula')?.value,
        fechaCreacion: this.comentario.fechaCreacion
      }
      this._comentario.updateDataCliente(this.id, Comentario).subscribe(data => { this.router.navigate(['/']); }, error => { console.log(error); })
    }


  }
}
