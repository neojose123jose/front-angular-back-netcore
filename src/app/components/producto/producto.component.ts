import { Component, OnInit } from '@angular/core';
import { Comentario } from 'src/app/interfaces/comentario';
import { ComentariosService } from 'src/app/services/comentarios.service';

@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html',
  styleUrls: ['./producto.component.css']
})
export class ProductoComponent implements OnInit {

  listComentarios: Comentario[] = [];


  constructor(private _info: ComentariosService) { }

  ngOnInit(): void {
    this.obtenerInfor();
  }

  obtenerInfor() {
    this._info.getListValuesProducto().subscribe(data => {
    //  console.log(data);
      this.listComentarios = data;
    }, error => {
      console.log(error);
    })
  }
  eliminarDato(id: number) {
    console.log(id);
    this._info.delateDataProducto(id).subscribe(data => { this.obtenerInfor(); }, error => { console.log(error); })
  }
}
