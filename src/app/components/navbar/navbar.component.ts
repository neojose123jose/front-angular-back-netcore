import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  img = 'https://www.unipymes.com/wp-content/uploads/2014/12/4.-DigitalWare.png';
  constructor() { }

  ngOnInit(): void {
  }

}
