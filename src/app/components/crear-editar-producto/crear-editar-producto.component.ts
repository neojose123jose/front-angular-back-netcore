import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
//import { ToastrService } from 'ngx-toastr';
import { producto } from 'src/app/interfaces/comentario';
import { ComentariosService } from 'src/app/services/comentarios.service';


@Component({
  selector: 'app-crear-editar-producto',
  templateUrl: './crear-editar-producto.component.html',
  styleUrls: ['./crear-editar-producto.component.css']
})
export class CrearEditarProductoComponent implements OnInit {
  action: string;
  id: number;
  producto: producto | undefined;

  agregarComentario: FormGroup;

  constructor(private fb: FormBuilder,
    private _comentario: ComentariosService,
    private router: Router,
    private aRoute: ActivatedRoute) {
    this.agregarComentario = this.fb.group({
      nombre: ['', Validators.required],
      precio: ['', Validators.required],
      cantidad: ['', Validators.required]

    })
    this.id = +this.aRoute.snapshot.paramMap.get('id')!;
  }

  ngOnInit(): void {
    // this.obtenerInfor();
      this.doEdit();
  }

  doEdit() {
    if (this.id !== 0) {
      this.action = "Editar";
      this._comentario.getIdProducto(this.id).subscribe(data => {
        this.producto = data;
        this.agregarComentario.patchValue({
          nombre: data.nombre,
          precio: data.precio,
          cantidad: data.cantidad
        })
      })
    } else {

      this.action = "Egregar";
    }
  }
  /*    obtenerInfor() {
      this._comentario.getListValues().subscribe(data => {
        console.log(data);
      }, error => {
        console.log(error);
      })
    }  */

  AddOrEdit() {
    //console.log(this.agregarComentario);
    if (this.producto == undefined) {
      const Comentario: producto = {
        nombre: this.agregarComentario.get('nombre')?.value,
        precio: this.agregarComentario.get('precio')?.value,
        cantidad: this.agregarComentario.get('cantidad')?.value,
        fechaCreacion: new Date
      }
      this._comentario.saveDataProducto(Comentario).subscribe(data => { this.router.navigate(['/']); }, error => { console.log(error); })

      console.log(Comentario);
    } else {
      const Comentario: producto = {
        id: this.producto.id,
        nombre: this.agregarComentario.get('nombre')?.value,
        precio: this.agregarComentario.get('precio')?.value,
        cantidad: this.agregarComentario.get('cantidad')?.value,
        fechaCreacion: this.producto.fechaCreacion
      }
      this._comentario.updateDataProducto(this.id, Comentario).subscribe(data => { this.router.navigate(['/']); }, error => { console.log(error); })
    }


  }
}
