import { Component, OnInit } from '@angular/core';
import { Comentario } from 'src/app/interfaces/comentario';
import { ComentariosService } from 'src/app/services/comentarios.service';
@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.css']
})
export class ClientesComponent implements OnInit {


  listComentarios: Comentario[] = [];


  constructor(private _info: ComentariosService) { }

  ngOnInit(): void {
    this.obtenerInfor();
  }

  obtenerInfor() {
    this._info.getListValuesCliente().subscribe(data => {
     // console.log(data);
      this.listComentarios = data;
    }, error => {
      console.log(error);
    })
  }
  eliminarDato(id: number) {
    console.log(id);
    this._info.delateDataCliente(id).subscribe(data => { this.obtenerInfor(); }, error => { console.log(error); })
  }

}
