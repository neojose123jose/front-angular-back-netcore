import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';

import { AppRoutingModule } from './app-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { DxButtonModule, DxFormModule } from 'devextreme-angular';
import { ProductoComponent } from './components/producto/producto.component';
import { InicioComponent } from './components/inicio/inicio.component';
import { ClientesComponent } from './components/clientes/clientes.component';
import { CrearEditarClienteComponent } from './components/crear-editar-cliente/crear-editar-cliente.component';
import { CrearEditarProductoComponent } from './components/crear-editar-producto/crear-editar-producto.component';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    ProductoComponent,
    InicioComponent,
    ClientesComponent,
    CrearEditarClienteComponent,
    CrearEditarProductoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    DxButtonModule,
    DxFormModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
