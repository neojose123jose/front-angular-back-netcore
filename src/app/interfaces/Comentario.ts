export interface Comentario {
    id?: number;
    nombre: string;
    apellido: string;
    cedula: number;
    fechaCreacion: Date;
}
export interface cliente {
    id?: number;
    nombre: string;
    apellido: string;
    cedula: number;
    fechaCreacion: Date;
}
export interface producto {
    id?: number;
    nombre: string;
    precio: number;
    cantidad: number;
    fechaCreacion: Date;
}